%{
% Get a table of beta, the output of BrainVoyager
% in csv or xslx format
% Output each subject on a row of their own
% Output each condition on a columb
% Allow means of a condition set to be computed as an additional
% condition
%}

outfile_path = "./out.csv";

[file, path] = uigetfile({'*.xlsx'; '*.csv'}, 'Select the Betas file...', 'Betas.xlsx');
%fprintf('path to file: %s\nfile: %s\n', path, file);

[betas, beta_txt, raw_data] = xlsread(strcat(path,file));

% Process the text to get the input conditions

% The first row of text contains the ROIs
ROI_names = beta_txt(1,:);
ROI_names(cellfun('isempty',ROI_names)) = []; %remove empty cells

% Which conditions to Process

%Get the condition names
condition_map = containers.Map();
conditions = {};
exp = '\d+\s+Subject\s+(\d+):\s+<(.+)>'; %regular expression to parse conditions

%Subjects by ID; count and aggregate
subject_map = containers.Map();
subjects = {};

%Get all subjects and conditions from raw data
for i = 2:length(beta_txt)
    %get the string from the excel cell
    txt = beta_txt(i,1);
    str = txt{1};
    %parse the string into subject number and condition tokens
    [s,e, token_i] = regexp(txt, exp);
    tokens = token_i{1};
    t_m = tokens{1};
    subject = str(t_m(1,1):t_m(1,2));
    condition = str(t_m(2,1):t_m(2,2));
    if (not(condition_map.isKey(condition)))
        conditions = [conditions condition];
    end
    if (not(subject_map.isKey(subject)))
        subjects = [subjects subject];
    end
    %Add the condition parsed into a collection (later to be aggregated)
    condition_map(condition) = 1;
    subject_map(subject) = 1;
end


out_file = fopen(outfile_path, "w");

%Write to file - All the titles, concating condition name with ROI
fprintf(out_file, "Subject, ");
for i = 1:length(conditions)
    condition = conditions{i};
    fprintf(out_file, "%s, %s_mean, ", strjoin(strcat(condition, "_", ROI_names), ", "), condition);
end
fprintf(out_file, "\n");

% prepare the data in the format so that each subject has their own line
%Write all the raw data and means per condition
condition_count = length(conditions);
for i = 1:length(subjects)
    subject = subjects{i};
    start_index = (i-1)*condition_count +1;
    fprintf(out_file, "%s, ", subject);
    for j = start_index:start_index+condition_count-1
        cur_condition = strjoin(arrayfun(@num2str, betas(j,:), 'UniformOutput', false), ", ");
        fprintf(out_file, "%s, ", cur_condition); 
        fprintf(out_file, "%s, ", mean(betas(j,:))); 
    end
    fprintf(out_file, "\n");
end
fprintf(out_file, "\n");

fclose(out_file);


% Loop to add mean aggregates of conditions
